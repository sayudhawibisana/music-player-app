# music player app
### The purposed of this app is to complete BCG code test. 
This simple music app is created simple with as minimal as possible using third party javascript bundle. I did not use any npm package, javascript framework, or setting up any CI CD to build and test in repository. 

As per mentioned on email I code this to showed off my skills, and I build this boilerplate and the webapp using jquery library only.



## How to use

1. Install Prepros on https://prepros.io/ to try run locally.
2. Checkout to development branch
3. All source codes are inside src folder, All build codes are inside dist folder
4. Right click on index.html inside dist folder and choose preview in browser


## NOTES
`If running using prepros got CORS policy by the API you can run it manually buy opening index.html`

